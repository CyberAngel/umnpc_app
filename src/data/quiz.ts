export interface quiz{
    id: string,
    theQuestion:string,
    choice_1: string,
    choice_2: string,
    choice_3: string,
    choice_4: string,
    answer: string
}