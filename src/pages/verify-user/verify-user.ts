import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserService} from '../../service/UserService';
import { AuthService } from '../../service/AuthService';

/**
 * Generated class for the VerifyUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify-user',
  templateUrl: 'verify-user.html',
})
export class VerifyUserPage {

  users = [];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService,
              public alertCtrl: AlertController, public authService: AuthService) {
    this.users = userService.show_all_registered_users();
    console.log(this.users);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyUserPage');
  }

  showPage(){
    console.log("Clicked");
  }

  popPage(){
    this.navCtrl.pop();
  }

  onShowAlert(user){
    const alert = this.alertCtrl.create({
      title: 'Verify User',
      message: 'Are you sure want to accept this user become UMNPC member? <br/>' + 
              'Name: ' + user.first_name + ' ' + user.last_name + '<br/>' + 
              'Email: ' + user.email + '<br/>' +
              'CF Handle: ' + user.cf_handle + '<br/>',
      buttons: [
        {
          text: 'Confirm',
          handler: () => {
            //add record to database user
            this.userService.add_user(user);
            this.authService.register(user.email, user.password);
            this.users = this.userService.show_all_registered_users();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
          }
        }
      ]
    });

    alert.present();
  }

}
