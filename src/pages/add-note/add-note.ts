import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertCmp, AlertController} from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '../../../node_modules/@angular/forms';
import { NoteService } from '../../service/NoteService';
import { Note } from '../../data/notes';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { User } from '../../service/user.interface';
import { TimelineService } from "../../service/TimelineService";
import { TimelineDetailPage } from "../timeline-detail/timeline-detail";
import { EventService } from "../../service/EventService";

/**
 * Generated class for the AddNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-note',
  templateUrl: 'add-note.html',
})
export class AddNotePage implements OnInit{
  newNoteForm: FormGroup;
  
  // currDate:any=new Date().toString();
  notes = [];
 
  constructor(public eventService:EventService, public navCtrl: NavController, public userService: UserService,
              public authService: AuthService, public navParams: NavParams, private viewCtrl: ViewController, 
              private noteService: NoteService, public timelineService: TimelineService,
              public alertCtrl: AlertController) {
  
  }

  addNotes(){
    let newData = this.newNoteForm.value;
    // let id = this.noteService.getNoteLength();
    console.log("ID : " + newData.title);
    let uemail=this.authService.getCurrentUserEmail();
    console.log('email='+uemail);
    let user=this.userService.get_current_user_id_byemail(uemail);
    let username = this.userService.get_user_username(user);
    newData.author = username;
    let sentence = newData.text;

    // let id = this.timelineService.show_all_post().length;
    // let id = this.timelineService.generateId();

    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let currDate = new Date().toLocaleDateString('en-US', options);

    this.timelineService.add_post(newData.title, username, sentence, currDate);
    // this.noteService.addNotes(user, newData.title, newData.author, newData.text);

    this.viewCtrl.dismiss();
  }

  cancel(){
    this.viewCtrl.dismiss();
  }

  ngOnInit(){
    this.newNoteForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      // author: new FormControl(null, Validators.required),
      text: new FormControl(null, Validators.required)
    })
  }

  showAlert(){
    const alert = this.alertCtrl.create({
      title: 'Error',
      message: 'Are you sure want to share this post?',
      cssClass: 'custom-alertDanger',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            //add post
            this.addNotes();
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
          }
        }
      ]
    });

    alert.present();
  }

}
