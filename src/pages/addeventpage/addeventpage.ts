import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '../../../node_modules/@angular/forms';
import { NoteService } from '../../service/NoteService';
import { Note } from '../../data/notes';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { User } from '../../service/user.interface';
import { TimelineService } from "../../service/TimelineService";
import { TimelineDetailPage } from "../timeline-detail/timeline-detail";
import { EventService } from "../../service/EventService";
/**
 * Generated class for the AddeventpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addeventpage',
  templateUrl: 'addeventpage.html',
})
export class AddeventpagePage {

  newEventForm: FormGroup;
  
  // currDate:any=new Date().toString();
  events = [];
 
  constructor(public eventService:EventService, public navCtrl: NavController, public userService: UserService,public authService: AuthService, public navParams: NavParams, private viewCtrl: ViewController, private noteService: NoteService, public timelineService: TimelineService) {
  
  }

  addEvents(){
    let newData = this.newEventForm.value;
    // let id = this.noteService.getNoteLength();
    console.log("ID : " + newData.title);
    let uemail=this.authService.getCurrentUserEmail();
    console.log('email='+uemail);
    let user=this.userService.get_current_user_id_byemail(uemail);
    let username = this.userService.get_user_username(user);
    newData.author = username;

    //let id = this.timelineService.show_all_post().length;
    let id = this.eventService.generateId();
    // let id = 1;
    console.log(id);

    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let currDate = new Date().toLocaleDateString('en-US', options);

    this.eventService.add_event(id, newData.title, username, newData.text, newData.start_time, newData.end_time, newData.location);
    // this.noteService.addNotes(user, newData.title, newData.author, newData.text);

    this.viewCtrl.dismiss();
  }

  cancel(){
    this.viewCtrl.dismiss();
  }

  ngOnInit(){
    this.newEventForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      // author: new FormControl(null, Validators.required),
      text: new FormControl(null, Validators.required),
      start_time: new FormControl(null, Validators.required),
      end_time: new FormControl(null, Validators.required),
      location: new FormControl(null, Validators.required)
    })
  }

}
