import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDailyProblemPage } from './admin-daily-problem';

@NgModule({
  declarations: [
    AdminDailyProblemPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDailyProblemPage),
  ],
})
export class AdminDailyProblemPageModule {}
