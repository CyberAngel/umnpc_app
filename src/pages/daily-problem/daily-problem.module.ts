import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyProblemPage } from './daily-problem';

@NgModule({
  declarations: [
    DailyProblemPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyProblemPage),
  ],
})
export class DailyProblemPageModule {}
