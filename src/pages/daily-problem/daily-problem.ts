import { DailyProblemService } from './../../service/DailyProblemService';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';



@IonicPage()
@Component({
  selector: 'page-daily-problem',
  templateUrl: 'daily-problem.html',
})
export class DailyProblemPage implements OnInit {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController,public authSrv: AuthService,public userSrv:UserService,public dailyProblemServ:DailyProblemService) {
  }

  anotherProblem:number;
  problems:string;
  userId:string;
  

  ngOnInit() {
    let uemail=this.authSrv.getCurrentUserEmail();
    this.userId=this.userSrv.get_current_user_id_byemail(uemail);
    this.anotherProblem=parseInt(this.userSrv.get_user_randomChance(this.userId));
    this.problems=this.dailyProblemServ.get_current_daily_problem(this.userId);
    
  }
  generateProblem(){
    let problem=Math.floor(Math.random() * ((1080-1)+1) + 1)+Math.floor(Math.random() * ((15-10)+1) + 10).toString(36).toUpperCase();
    return problem;
   }

   problemListIfzero(){
    
       let alert = this.alertCtrl.create({
         title: 'Your change is gone take it or leave it',
         subTitle:this.problems,
         buttons: [
           {
             text:'done',
             handler: () => {
               let status= this.problems+' Solved';
              this.dailyProblemServ.update_user_daily_problem(this.userId,status);
             }
           },
           {
             text:'Fine...',
             handler: () => {
              
             }
           }
         ]
       });
       alert.present();
     }
   

   problemList(){
    
    if(this.anotherProblem>0){
      this.problems= this.generateProblem();
      this.dailyProblemServ.update_user_daily_problem(this.userId,this.problems);
      let alert = this.alertCtrl.create({
        title: '',
        message: this.problems,
        buttons: [
          {
            text:'Another problem('+this.anotherProblem+')',
            handler: () => {
              this.anotherProblem=this.anotherProblem-1;
              this.problemList();
            }
          },
          {
            text: 'solved!',
            handler: () => {
              // increase score
            }
          }
        ]
      });
      alert.present();
    }
    
  }

  dailyProblem(){

    
   if(this.anotherProblem<0||this.anotherProblem==0)
    {
      console.log(this.anotherProblem);
      this.problems=this.dailyProblemServ.get_current_daily_problem(this.userId);
      this.problemListIfzero();
    }
    else if(this.anotherProblem>0){
      this.anotherProblem=this.anotherProblem-1;
      let x= this.anotherProblem.toString();
      this.userSrv.update_user_randomChance(this.userId,x);
      this.anotherProblem=parseInt(this.userSrv.get_user_randomChance(this.userId));
      console.log('another problem= '+this.anotherProblem);
      this.problemList();
    }
    
  }
}
