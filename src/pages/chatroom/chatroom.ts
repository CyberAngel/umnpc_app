import { LocalNotifications } from '@ionic-native/local-notifications';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import * as moment from 'moment';
/**
 * Generated class for the ChatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatroom',
  inputs: ['placeholder', 'lineHeight'],
  
  templateUrl: 'chatroom.html',
})
export class ChatroomPage {
  username='';
  message='';
  time='';
  subscription;
  times : object[]=[];
  messages : object[]=[];
  myDate: String = new Date().toISOString();

  constructor(public db:AngularFireDatabase ,public navCtrl: NavController, public navParams: NavParams, public authService:AuthService, public userService:UserService,public platform:Platform, public localNotif:LocalNotifications) {
    console.log(this.navParams);
    // this.username=this.navParams.get('username');
    let uemail=this.authService.getCurrentUserEmail();
    console.log('email='+uemail);
    let user=this.userService.get_current_user_id_byemail(uemail);
    let name = this.userService.get_user_username(user);
    this.username = name;

    this.subscription=this.db.list('/chat').valueChanges().subscribe(data =>
      {
        this.messages=data;
        this.resetSendNews();
        // this.times=data;
      });
  }

  resetSendNews(){
    //console.log('mulai notif');
    this.platform.ready().then(() => {
      this.localNotif.schedule({
      title:'You Have new Message ',
      text: 'You Have new Message',
      actions: [
        { id: 'yes', title: 'Yes' },
        { id: 'no',  title: 'No' }
    ]
      });
      //this.navCtrl.push(RandomPage);
    });
  }

  sendMessage()
{
  this.time = moment(this.navParams.get('selectedDay')).format();
  this.db.list('/chat').push(
    {
      username: this.username,
      message :this.message,
      time: this.time
    }
  ).then(()=>
    {

    }
  );
  this.message=' ';
}

ionViewWillLeave()
{
  this.time = moment(this.navParams.get('selectedDay')).format();
  console.log('user is about to go');
  this.subscription.unsubscribe();
  this.db.list('/chat').push(
    {
      specialMessage:true,
      message:`${this.username} has left the room`,
      time: this.time
    }
  )
}
ionViewDidLoad()
{
  this.time = moment(this.navParams.get('selectedDay')).format();
  this.db.list('/chat').push(
    {
      specialMessage:true,
      message:`${this.username} has joined the room`,
      time: this.time

    }
  )
};

}

