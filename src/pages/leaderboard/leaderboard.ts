import { AuthService } from './../../service/AuthService';
import { UserService } from './../../service/UserService';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LeaderboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leaderboard',
  templateUrl: 'leaderboard.html',
})
export class LeaderboardPage implements OnInit {

  constructor(public navCtrl: NavController, public navParams: NavParams,public userServ:UserService,public authSrv:AuthService) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaderboardPage');
  }
  rank1:any;
  rank2:any;
  rank3:any;
  rank4:any;
  rank5:any;

  namaRank1:any[];
  namaRank2:any[];
  namaRank3:any[];
  namaRank4:any[];
  namaRank5:any[];

  ngOnInit() {



    
    let UniqueNumber=[];
    let number =[];
    let member=[];
    let x1=[];
    let x2=[];
    let x4=[];
    let x3=[];
    let x5=[];
    this.userServ.load_all_members();
    member=this.userServ.show_all_member();
   
    for(let x=0;x<this.userServ.get_total_member();x++)
    {
    
      number.push(member[x].score);
    }

    
    number=number.sort((a, b) => b - a);
    console.log(number);
    
    let emails=this.authSrv.getCurrentUserEmail();
    let userID=this.userServ.get_current_user_id_byemail(emails);
    this.pointUser=this.userServ.get_user_score(userID);
    this.namaUSer=this.userServ.get_user_username(userID);

    let xq=1;
    let stop='no';
    for(let x=0;x<this.userServ.get_total_member();x++)
    {
      if(number[x]>number[x+1]){
        UniqueNumber.push(number[x]);
        if(number[x]==this.pointUser){
          this.rankUser=xq;
        }
        xq=xq+1;
      }
      if(number[x]==0 &&stop=='no' ){
        stop='w';
        UniqueNumber.push(number[x]);
        //xq=xq+1;
        if(number[x]==this.pointUser){
          this.rankUser=xq;
        }
      }
    }

    console.log('sssssssss '+number);
    console.log(UniqueNumber);

    this.rank1=Math.max.apply(null,number);
   
    number=UniqueNumber;
    var index = number.indexOf(this.rank1);
    if (index > -1) {
      number.splice(index, 1);
    }
    //console.log(number);
    this.rank2=Math.max.apply(null,number);
    

    index = number.indexOf(this.rank2);
    if (index > -1) {
      number.splice(index, 1);
    }
    //console.log(number);
    this.rank3=Math.max.apply(null,number);
    
    index = number.indexOf(this.rank3);
    if (index > -1) {
      number.splice(index, 1);
    }
    console.log('4 '+number);
    this.rank4=Math.max.apply(null,number);
    
    index = number.indexOf(this.rank4);
    if (index > -1) {
      number.splice(index, 1);
    }
    console.log('5 '+number);
    this.rank5=Math.max.apply(null,number);
    console.log('rank5 '+this.rank5);

    for(let x=0;x<this.userServ.get_total_member();x++)
    {
      
      if(member[x].score==this.rank1)
      {
        x1.push(member[x].first_name+member[x].last_name);
      }
      if(member[x].score==this.rank2)
      {
        x2.push(member[x].first_name+member[x].last_name);
      }
      if(member[x].score==this.rank3)
      {
        x3.push(member[x].first_name+member[x].last_name);
      }
      if(member[x].score==this.rank4)
      {
        x4.push(member[x].first_name+member[x].last_name);
      }
      if(member[x].score==this.rank5)
      {
        x5.push(member[x].first_name + member[x].last_name);
      }
    }
    this.namaRank1=x1;
    this.namaRank2=x2;
    this.namaRank3=x3;
    this.namaRank4=x4;
    this.namaRank5=x5;
  
  }
    namaUSer:any;
    rankUser:any;
    pointUser:any;

}
