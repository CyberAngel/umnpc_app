import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddSoalPage } from './add-soal';

@NgModule({
  declarations: [
    AddSoalPage,
  ],
  imports: [
    IonicPageModule.forChild(AddSoalPage),
  ],
})
export class AddSoalPageModule {}
