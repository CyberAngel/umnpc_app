import { EventService } from '../../service/EventService';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast } from 'ionic-angular';
import { TimelineService } from "../../service/TimelineService";
import { TimelineDetailPage } from "../timeline-detail/timeline-detail";

import { AuthService } from '../../service/AuthService';
import { UserService } from '../../service/UserService';
import { User } from '../../service/user.interface';

import { AlertController, ModalController, ToastController } from 'ionic-angular';
import { NoteService } from '../../service/NoteService';
import { AddNotePage } from '../add-note/add-note';
import { DetailsPage } from '../details/details';
import { Note } from '../../data/notes';
import { CommentPage } from '../comment/comment';
import { CommentlistPage } from '../commentlist/commentlist'
import { CommentService } from '../../service/CommentService';
import { AngularFireDatabase } from "angularfire2/database";
import firebase from 'firebase';
import { AddeventpagePage } from '../addeventpage/addeventpage';
import { EventdetailsPage } from "../eventdetails/eventdetails"

/**
 * Generated class for the EventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage implements OnInit{

  public events: any;
  allEvents = [];

  constructor(public db:AngularFireDatabase, public authService: AuthService, public timelineService: TimelineService, 
    public userService: UserService, public modalCtrl: ModalController, public noteService: NoteService, public toastCtrl: ToastController,
    public commentService: CommentService, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public eventService: EventService) {
    // this.events = eventService.getAll();
    // console.log(this.events.id_event);
    this.eventService.load_all_events();
    this.allEvents = this.eventService.show_all_event();
    // console.log(this.allEvents);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventPage');
    console.log(this.allEvents);
  }

  ngOnInit(){
    this.allEvents = this.eventService.show_all_event();
  }

  viewDetail(event) 
  {
    this.navCtrl.push(EventdetailsPage, {event: event});
  // submit_logout(){
  //   this.authService.logout();
  // }
  }

  createEvent(){
    //add record to database
    let modal = this.modalCtrl.create(AddeventpagePage);
    modal.onDidDismiss(()=>{
      this.allEvents = this.eventService.show_all_event();
    })

    modal.present();
  }

  is_adminss(){
    let user_now = this.authService.getCurrentUser();

    if(user_now == null) return false;
    if(user_now.email == "umnpc@umnpc.com") return true;
    return false;
  }

  openModalDelete(event){

    const alert = this.alertCtrl.create({
      title: 'Delete Event',
      message: 'Are you sure want to delete this event?',
      cssClass: 'custom-alertDanger',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.eventService.delete_event(event.id);
            this.allEvents = this.eventService.show_all_event();
            console.log(this.allEvents);

            let toast = this.toastCtrl.create({
              message: 'Post has been deleted',
              duration: 2000,
              position: 'bottom'
            });
          
            toast.present();
          }
        },
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'custom-alertDanger',
          handler: () => {
            console.log("Cancel is clicked!");
          }
        }
      ]
    });

    alert.present();
  }

}
