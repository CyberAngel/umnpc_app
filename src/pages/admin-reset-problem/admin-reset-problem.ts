import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserService } from '../../service/UserService';

/**
 * Generated class for the AdminResetProblemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-reset-problem',
  templateUrl: 'admin-reset-problem.html',
})
export class AdminResetProblemPage {

  users = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService,
              public alertCtrl: AlertController) {
    this.users = this.userService.show_all_member();
  }

  popPage(){
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminResetProblemPage');
  }

  resetProblem(user){
    const alert = this.alertCtrl.create({
      title: 'Reset Daily Problem',
      message: 'Are you sure to reset ' +  user.first_name + ' ' + user.last_name + ' daily problem?<br/>',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            //reset probem here
            console.log("Reset Problem");
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked!");
          }
        }
      ]
    });

    alert.present();
  }

}
