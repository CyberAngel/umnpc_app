import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserService } from '../../service/UserService';

/**
 * Generated class for the ViewComiteePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-comitee',
  templateUrl: 'view-comitee.html',
})
export class ViewComiteePage {

  public users: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService,
              public alertCtrl: AlertController) {
    this.users = userService.show_all_committee();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewComiteePage');
  }

  popPage(){
    this.navCtrl.pop();
  }

  onShowAlertTurunkanPengurus(user){
    const alert = this.alertCtrl.create({
      title: 'Change Status to Member',
      message: 'Are you sure want to promote this user to commitee? <br/>' +
              'Name: ' + user.first_name + ' ' + user.last_name + '<br/>' + 
              ' Email: ' + user.email + '<br/>',
      buttons: [
        {
          text: 'Ya',
          handler: () => {
            //ganti status user
            this.userService.to_member(user.id);
            this.users = this.userService.show_all_committee();
          }
        },
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
            console.log(user.id);
          }
        }
      ]
    });

    alert.present();
  }

  onShowAlertDelete(user){
    const alert = this.alertCtrl.create({
      title: 'Delete User',
      message: 'Are you sure want to delete this user? <br/>' +
              'Name: ' + user.first_name + ' ' + user.last_name + '<br/>' + 
              ' Email: ' + user.email + '<br/>',
      buttons: [
        {
          text: 'Ya',
          handler: () => {
            //delete user
            this.userService.delete_user(user.id);
            this.users = this.userService.show_all_committee();
          }
        },
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log("Cancel is clicked");
          }
        }
      ]
    });

    alert.present();
  }

}
