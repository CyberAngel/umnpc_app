import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewComiteePage } from './view-comitee';

@NgModule({
  declarations: [
    ViewComiteePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewComiteePage),
  ],
})
export class ViewComiteePageModule {}
