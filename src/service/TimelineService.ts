import {Injectable} from "@angular/core";
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';
import { Note } from "../data/notes";


export class TimelineService 
{
  posts = [];
  allNotes = [];
  id;

  load_all_posts()
  {
    this.allNotes = [];
    firebase.database().ref('pc_post').on('value', (snapshot) => {
        let result = snapshot.val();
        for(let k in result) {
            this.allNotes.push({
                id : k,
                // id_post: result[k].id_post,
                author: result[k].id_user,
                text: result[k].statement,
                time: result[k].time,
                title: result[k].title,
                // comment_counter: result[k].comment_counter
            });
        }
    });
  }

  show_all_post(){
    return this.allNotes;
  }

  getPostLength(){
    return this.allNotes.length;
  }

  // generateId(){
  //   for(var i = 0; i< this.allNotes.length ; i++){
  //     this.id = this.allNotes[i].id_post;
  //   }

  //   return this.id + 1;

  // }

  // show_all_posts()
  // {
  //   return this.posts;
  // }

  add_post(title: string, id_user: string, sentence: string, time_post: string) {
    var new_post = {
      id_user : id_user,
      statement: sentence,
      like_counter: 0,
      comment_counter: 0,
      time: time_post,
      title: title,
    };

    firebase.database().ref('pc_post').push(new_post);
    this.load_all_posts();
  }

  get_post(id: string)
  {
    let post;
    firebase.database().ref('pc_post/' + id).once("value", (snapshot) => {
      post = snapshot.val();
    });

    return post;
  }

  delete_post(id: string) 
  {
    firebase.database().ref('pc_post/' + id).remove();
    this.load_all_posts();
  }

}