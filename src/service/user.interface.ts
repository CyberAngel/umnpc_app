export interface User{
    first_name : string,
    last_name : string,
    email : string,
    cf_handle : string,
    password : string,
    status: 0,
    random_chance: 3,
    score: 0
}
