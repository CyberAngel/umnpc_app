import firebase from 'firebase';

export class EventService 
{
  allEvents = [];
  id;

  load_all_events()
  {
    this.allEvents = [];
    firebase.database().ref('pc_event').on('value', (snapshot) => {
        let result = snapshot.val();
        for(let k in result) {
            this.allEvents.push({
                id : k,
                id_event: result[k].id_event,
                author: result[k].id_user,
                text: result[k].text,
                start_time: result[k].start_time,
                title: result[k].title,
                end_time: result[k].end_time,
                location: result[k].location
                // comment_counter: result[k].comment_counter
            });
        }
    });
  }

  show_all_event(){
    return this.allEvents;
  }
  getAll() {
    // return this.event;
  }

  getItem(id) {
    // for (var i = 0; i < this.event.length; i++) {
    //   if (this.event[i].id === parseInt(id)) {
    //     return this.event[i];
    //   }
    // }
    // return null;
  }

  remove(item) {
    // this.event.splice(this.event.indexOf(item), 1);
  }

  getEventLength(){
    return this.allEvents.length;
  }

  generateId(){
    for(var i = 0; i< this.allEvents.length ; i++){
      this.id = this.allEvents[i].id_event;
    }

    return this.id + 1;

  }

  // show_all_posts()
  // {
  //   return this.posts;
  // }

  add_event(id_event:number, title: string, id_user: string, text: string, start_time: string, end_time: string, location: string) {
    var new_event = {
      id_user: id_user,
      id_event: id_event,
      title: title,
      start_time: start_time,
      end_time: end_time,
      location: location,
      text: text
    };

    firebase.database().ref('pc_event').push(new_event);
    this.load_all_events();
  }

  get_event(id: string)
  {
    let event;
    firebase.database().ref('pc_event/' + id).once("value", (snapshot) => {
      event = snapshot.val();
    });

    return event;
  }

  delete_event(id: string) 
  {
    firebase.database().ref('pc_event/' + id).remove();
    this.load_all_events();
  }
}