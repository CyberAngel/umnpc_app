import { Question } from './mock-question';
import { AngularFireDatabase } from 'angularfire2/database';
import {Injectable} from "@angular/core";
import firebase from 'firebase';
import { DAILYPROBLEM } from './mock-daily-problem';
import { Http ,HttpModule} from '@angular/http';

@Injectable()
export class DailyQuizService
{

    id;
    soalQuiz=[];
    lengthss:any;
    get_all_quiz(){
        firebase.database().ref('pc_quiz').on('value', (snapshot) => {
            let result = snapshot.val();
            let x=-1;
            for(let k in result) {
                x=x+1;
                this.soalQuiz.push({
                    id:k,
                    id_quiz:result[k].id_quiz,
                    jawaban:result[k].jawaban,
                    pertanyaan:result[k].pertanyaan,
                    pilihan1:result[k].pilihan1,
                    pilihan2:result[k].pilihan2,
                    pilihan3:result[k].pilihan3,
                    pilihan4:result[k].pilihan4
                });
            }
            this.lengthss=x;
            console.log(this.soalQuiz);
            return this.soalQuiz;
            //console.log(this.soalQuiz);
        });
        return this.soalQuiz;
    
    }

    show_all_quiz(){
        return this.soalQuiz;
    }

    
    update_length(){

        for(var i = 0; i< this.soalQuiz.length ; i++){
             this.id = this.soalQuiz[i].id_quiz;
          }
      
          return this.id + 1;
        // let x=0
        // firebase.database().ref('pc_quiz').on('value', (snapshot) => {
        //     let result = snapshot.val();
        //    ;
        //     for(let k in result) {
        //        x= result[k].id_quiz;
        //     }
        //     x=x+1;
        //     return x;
        //     //console.log(this.soalQuiz);
        // });
        // return x;
        
    
    }
    get_length_soal(){
        return this.lengthss;
    }

    update_user_quizStatus(id:string,problem:string)
    {
        firebase.database().ref('pc_users/' + id).update(
            { 
                daily_problem:problem
            }
        );
    }

    add_quiz(id:number, jawaban: string, pertanyaan: string,pilihan1: string,pilihan2: string,pilihan3: string,pilihan4: string) {
        var new_post = {
            id_quiz:id,
            jawaban:jawaban,
            pertanyaan:pertanyaan,
            pilihan1:pilihan1,
            pilihan2:pilihan2,
            pilihan3:pilihan3,
            pilihan4:pilihan4
        };
    
        firebase.database().ref('pc_quiz').push(new_post);
       
      }
      add_quiz123(value) {
        let id= this.update_length();
        var new_post = {
            id_quiz:id,
            jawaban:value.answer,
            pertanyaan:value.question,
            pilihan1:value.option_a,
            pilihan2:value.option_b,
            pilihan3:value.option_c,
            pilihan4:value.option_d
        };
    
        firebase.database().ref('pc_quiz').push(new_post);
       
      }
    
}