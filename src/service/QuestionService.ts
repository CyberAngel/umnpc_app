import {Injectable} from "@angular/core";
import {Question} from "./mock-question";

@Injectable()
export class QuestionService {
  private question: any;

  constructor() {
    this.question = Question;
  }

  getAll() {
    return this.question;
  }

  getLength(){
    return this.question.length;
  }

  getItem(id) {
    for (var i = 0; i < this.question.length; i++) {
      if (this.question[i].id === parseInt(id)) {
        return this.question[i];
      }
    }
    return null;
  }

  remove(item) {
    this.question.splice(this.question.indexOf(item), 1);
  }
}