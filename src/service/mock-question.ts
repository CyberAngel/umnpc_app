export let Question = [
	{
		id:1,
		pertanyaan:"Ini adalah pertanyaan pertama",
		radio1:"Pilihan 1",
		radio2:"Pilihan 2",
		radio3:"Pilihan 3",
		radio4:"Pilihan 4",
		radio5:"Pilihan 5",
		answer:"Pilihan 3"
	},
	{
		id:2,
		pertanyaan:"Ini adalah pertanyaan kedua",
		radio1:"Pilihan 1",
		radio2:"Pilihan 2",
		radio3:"Pilihan 3",
		radio4:"Pilihan 4",
		radio5:"Pilihan 5",
		answer:"Pilihan 3"
	},
	{
		id:3,
		pertanyaan:"Ini adalah pertanyaan ketiga",
		radio1:"Pilihan 1",
		radio2:"Pilihan 2",
		radio3:"Pilihan 3",
		radio4:"Pilihan 4",
		radio5:"Pilihan 5",
		answer:"Pilihan 3"
	}
]