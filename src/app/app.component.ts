import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import firebase from "firebase";
import { LoginPage } from '../pages/login/login';
import { TimelinePage } from '../pages/timeline/timeline';
import { DailyQuizPage } from '../pages/daily-quiz/daily-quiz';
import { DailyProblemPage } from '../pages/daily-problem/daily-problem';
import { ChatPage } from '../pages/chat/chat';
import { SettingsPage } from '../pages/settings/settings';
import { ListMemberPage } from '../pages/list-member/list-member';
import { ListCommitteePage } from '../pages/list-committee/list-committee';
import { LeaderboardPage } from '../pages/leaderboard/leaderboard';
import { EventPage } from '../pages/event/event';
import { CalendarPage } from '../pages/calendar/calendar';
import { VerifyUserPage } from '../pages/verify-user/verify-user';
import { AdminPage } from '../pages/admin/admin';

import { AddNotePage } from '../pages/add-note/add-note';
import { DetailsPage } from '../pages/details/details';
import { NoteService } from '../service/NoteService';
import { CommentPage } from '../pages/comment/comment';
import { CommentService } from '../service/CommentService';
import { CommentlistPage } from '../pages/commentlist/commentlist';
import { ChatroomPage } from '../pages/chatroom/chatroom';
import { AddeventpagePage } from '../pages/addeventpage/addeventpage';
import { EventdetailsPage } from '../pages/eventdetails/eventdetails';

import { AuthService } from '../service/AuthService';
import { MiscService } from '../service/MiscService';
import { UserService } from '../service/UserService';
import { TimelineService } from '../service/TimelineService';

import { DatePipe } from '@angular/common';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  
  timelinePage = TimelinePage;
  dailyQuizPage = DailyQuizPage;
  dailyProblemPage = DailyProblemPage;
  chatPage = ChatPage;
  settingsPage = SettingsPage;
  listMemberPage = ListMemberPage;
  listCommitteePage = ListCommitteePage;
  leaderboardPage = LeaderboardPage;
  eventPage = EventPage;
  calendarPage = CalendarPage;
  verifyUserPage = VerifyUserPage;
  adminPage = AdminPage;
  detailsPage = DetailsPage;
  noteService = NoteService;
  commentPage = CommentPage;
  commentlistPage = CommentlistPage;
  chatroompage = ChatroomPage;
  addeventpagepage = AddeventpagePage;
  eventdetailsPage = EventdetailsPage;


  @ViewChild('sideMenuContent') nav: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public menuCtrl: MenuController, 
              public authService: AuthService, public miscService: MiscService, public userService: UserService,
              public timelineService: TimelineService, public commentService: CommentService, private datePipe: DatePipe) {
    var config = {
      apiKey: "AIzaSyC7liL1RRqGWAsy1-YcMrwfKfOjZmSNmck",
      authDomain: "umnpc-app.firebaseapp.com",
      databaseURL: "https://umnpc-app.firebaseio.com",
      projectId: "umnpc-app",
      storageBucket: "umnpc-app.appspot.com",
      messagingSenderId: "851226009946"
    };

    firebase.initializeApp(config);

    this.userService.load_all_registered_users();
    this.userService.load_all_members();
    this.userService.load_all_committees();
    this.timelineService.load_all_posts();
    this.commentService.load_all_comment();
    
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if(!user){
        this.rootPage = LoginPage;
        this.menuCtrl.swipeEnable(false);
      } else {
        this.rootPage = TimelinePage;
        this.menuCtrl.swipeEnable(true);
      }
    })

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  show_page(page: any){
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  sign_out() {
    this.authService.logout();
    this.menuCtrl.close();
  }

  is_admin() {
    let user_now = this.authService.getCurrentUser();
    
    if(user_now == null) return false;
    if(user_now.email == "umnpc@umn.ac.id") return true;
    return false;
  }

  is_adminss(){
    let user_now = this.authService.getCurrentUser();

    if(user_now == null) return false;
    if(user_now.email == "umnpc@umnpc.com") return true;
    return false;
  }
}

